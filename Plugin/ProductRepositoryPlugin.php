<?php

declare(strict_types=1);

namespace Visma\ExtensionAttributes\Plugin;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\Data\ProductSearchResultsInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Visma\ExtensionAttributes\Api\ExtensionAttributesManagementInterface;

class ProductRepositoryPlugin
{
    /**
     * @var ExtensionAttributesManagementInterface $extensionAttributesManagementInterface
     */
    private ExtensionAttributesManagementInterface $extensionAttributesManagementInterface;

    /**
     * Constructor
     *
     * @param ExtensionAttributesManagementInterface $extensionAttributesManagementInterface
     */
    public function __construct(
        ExtensionAttributesManagementInterface $extensionAttributesManagementInterface
    ) {
        $this->extensionAttributesManagementInterface = $extensionAttributesManagementInterface;
    }

    public function afterGetList(
        ProductRepositoryInterface $subject,
        ProductSearchResultsInterface $productSearchResultsInterface
    ): ProductSearchResultsInterface {
        foreach ($productSearchResultsInterface->getItems() as $product) {
            $this->afterGet($subject, $product);
        }

        return $productSearchResultsInterface;
    }

    public function afterGet(
        ProductRepositoryInterface $subject,
        ProductInterface $product
    ): ProductInterface {
        $isProduct = (bool)$this->extensionAttributesManagementInterface->getByProductId((int)$product->getId());

        $extensionAttributes = $product->getExtensionAttributes();
        $extensionAttributes->setIsProduct($isProduct);
        $product->setExtensionAttributes($extensionAttributes);

        return $product;
    }

    public function afterSave(
        ProductRepositoryInterface $subject,
        ProductInterface $result,
        ProductInterface $product
    ): ProductInterface {
        $extensionAttributes = $product->getExtensionAttributes();

        $isProduct = $extensionAttributes->getIsProduct();

        $resultAttributes = $result->getExtentionAttributes();
        $resultAttributes->setIsProduct($isProduct);
        $result->setExtensionAttributes($resultAttributes);

        return $result;
    }
}
