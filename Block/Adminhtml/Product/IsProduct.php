<?php

declare(strict_types=1);

namespace Visma\ExtensionAttributes\Block\Adminhtml\Product;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;

class IsProduct extends Template
{
    /**
     * @var $_template
     */
    protected $_template = 'Visma_ExtensionAttributes::is-product.phtml';

    /**
     * @var Registry $registry
     */
    private Registry $registry;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        array $data = []
    ) {
        $this->registry = $registry;
        parent::__construct($context, $data);
    }

    public function getProduct()
    {
        return $this->registry->registry('product');
    }
}
