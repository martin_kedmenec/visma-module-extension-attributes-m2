<?php

declare(strict_types=1);

namespace Visma\ExtensionAttributes\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Visma\ExtensionAttributes\Api\ExtensionAttributesManagementInterface;

class IsProduct implements ArgumentInterface
{
    /**
     * @var ExtensionAttributesManagementInterface $extensionAttributesManagementInterface
     */
    private ExtensionAttributesManagementInterface $extensionAttributesManagementInterface;

    /**
     * Constructor
     *
     * @param ExtensionAttributesManagementInterface $extensionAttributesManagementInterface
     */
    public function __construct(
        ExtensionAttributesManagementInterface $extensionAttributesManagementInterface
    ) {
        $this->extensionAttributesManagementInterface = $extensionAttributesManagementInterface;
    }

    /**
     * Get valuable information from the database
     *
     * @param int $productId
     * @return string
     */
    public function getIsProduct(int $productId): string
    {
        if (!$this->extensionAttributesManagementInterface->getByProductId($productId)->isProduct()) {
            return 'No, it is not';
        }

        return 'Congratulations, this is a product!';
    }
}
