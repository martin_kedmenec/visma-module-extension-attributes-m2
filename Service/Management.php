<?php

declare(strict_types=1);

namespace Visma\ExtensionAttributes\Service;

use Visma\ExtensionAttributes\Api\Data\ExtensionAttributesInterface;
use Visma\ExtensionAttributes\Api\ExtensionAttributesManagementInterface;
use Visma\ExtensionAttributes\Model\ExtensionAttributesFactory;
use Visma\ExtensionAttributes\Model\ExtensionAttributesResource;

class Management implements ExtensionAttributesManagementInterface
{
    /**
     * @var ExtensionAttributesResource
     */
    private ExtensionAttributesResource $extensionAttributesResource;

    /**
     * @var ExtensionAttributesFactory
     */
    private ExtensionAttributesFactory $extensionAttributesFactory;

    /**
     * @param ExtensionAttributesResource $extensionAttributesResource
     * @param ExtensionAttributesFactory $extensionAttributesFactory
     */
    public function __construct(
        ExtensionAttributesResource $extensionAttributesResource,
        ExtensionAttributesFactory $extensionAttributesFactory
    ) {
        $this->extensionAttributesResource = $extensionAttributesResource;
        $this->extensionAttributesFactory = $extensionAttributesFactory;
    }

    /**
     * @param int $productId
     * @return ExtensionAttributesInterface
     */
    public function getByProductId(int $productId): ExtensionAttributesInterface
    {
        $object = $this->extensionAttributesFactory->create();
        $this->extensionAttributesResource->load($object, $productId, 'product_id');

        return $object;
    }
}
