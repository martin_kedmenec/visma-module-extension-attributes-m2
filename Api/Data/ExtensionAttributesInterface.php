<?php

declare(strict_types=1);

namespace Visma\ExtensionAttributes\Api\Data;

/**
 * @api
 */
interface ExtensionAttributesInterface
{
    /**
     * @return int
     */
    public function getProductId(): int;

    /**
     * @return bool
     */
    public function isProduct(): bool;
}
