<?php

declare(strict_types=1);

namespace Visma\ExtensionAttributes\Api;

use Visma\ExtensionAttributes\Api\Data\ExtensionAttributesInterface;

/**
 * @api
 */
interface ExtensionAttributesManagementInterface
{
    /**
     * @param int $productId
     * @return ExtensionAttributesInterface
     */
    public function getByProductId(int $productId): ExtensionAttributesInterface;
}
