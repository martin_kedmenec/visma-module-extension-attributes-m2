<?php

declare(strict_types=1);

namespace Visma\ExtensionAttributes\Model;

use Magento\Catalog\Model\ResourceModel\Collection\AbstractCollection;

class ExtensionAttributesCollection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(ExtensionAttributes::class, ExtensionAttributesResource::class);
    }
}
