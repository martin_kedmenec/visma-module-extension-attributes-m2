<?php

declare(strict_types=1);

namespace Visma\ExtensionAttributes\Model;

use Magento\Framework\Model\AbstractModel;
use Visma\ExtensionAttributes\Api\Data\ExtensionAttributesInterface;

class ExtensionAttributes extends AbstractModel implements ExtensionAttributesInterface
{
    private const PRODUCT_ID = 'product_id';
    private const IS_PRODUCT = 'is_product';

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return (int)$this->getData(self::PRODUCT_ID);
    }

    /**
     * @return bool
     */
    public function isProduct(): bool
    {
        return (bool)$this->getData(self::IS_PRODUCT);
    }

    protected function _construct()
    {
        $this->_init(ExtensionAttributesResource::class);
    }
}
