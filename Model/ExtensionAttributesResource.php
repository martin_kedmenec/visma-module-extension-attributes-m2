<?php

declare(strict_types=1);

namespace Visma\ExtensionAttributes\Model;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class ExtensionAttributesResource extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('visma_extension_attributes', 'entity_id');
    }
}
